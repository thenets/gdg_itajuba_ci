SHELL := /bin/bash

start:
	source venv/bin/activate && python src/main.py
	
prepare:
	virtualenv -p python3 venv && source venv/bin/activate && pip install -r requirements.txt

update-packages:
	source venv/bin/activate && pip install -r requirements.txt

test:
	source venv/bin/activate && python src/test_main.py