FROM alpine

# Main dependencies
RUN apk --no-cache add \
        python3 py3-pip git bash \
		python3-dev build-base gcc libxml2-dev libxslt-dev make
	
# Envs
ENV HOME=/app
ENV USER=luiz

# Setup Python
WORKDIR $HOME
RUN set -x \
    # Add user
    && adduser -D -u 1000 -s /bin/bash $USER \
    # Setup virtualenv
    && python3 -m pip install pip --upgrade \
    && python3 -m pip install virtualenv \
    # Set permissions
	&& chown -R 1000.1000 $HOME

# Copy files
ADD . $HOME
RUN make prepare

CMD ["make", "start"]