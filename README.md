# Continuous Integration

## Principais tópicos
- Ferramentas necessárias.
- Vantagens (auto-deploy, pull request check, vulnerability check...).
- Requisitos de uma aplicação para se usar CI (tests, container...).
- O que é uma pipeline.
- Criar um ambiente homogêneo.
- Construir aplicação.
- Executar testes.
- Notificações (Slack, IRC, email...).
- Integrações (Kubernetes, metrics, board...).
- Rotinas agendadas.

## Vantagens
- Permite muitos merges por dia
- Rotinas a cada commit
- Testes de unidade (ou testes unitários)
- Economia de tempo
- Garantia que um commit não quebrou uma feature

## Obrigações
- Criar testes automatizados organizados
- Manter as configurações da aplicação no repositório

## Exemplos
- Commit simples
- Pull request: https://github.com/thenets/EasyCKAN/pull/28
- Projeto com build contínuo: https://github.com/thenets/docker-parsoid
- Notificação: https://github.com/thenets/docker-parsoid/blob/master/.travis.yml#L15-L22

# Entrega contínua
- Integração do GitLab com o Kubernetes
- Utilizar a API de Rancher para integrar com qualquer CI