import logging

def getHelloMessage(msg="GDG"):
	if("GDG" not in msg):
		logging.error("Not a message for GDG!")
		exit(1)
	return 'Hello, '+msg